package resolution.example6.zzeulki.practice63;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class call_layout extends AppCompatActivity {

    TextView name2;
    TextView phone2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_layout);

        name2 = (TextView) findViewById(R.id.textView3);
        phone2 = (TextView) findViewById(R.id.textView4);

        name2.setText(MainActivity.N + " : " + MainActivity.name.getText().toString());
        phone2.setText(MainActivity.P + " : "  + MainActivity.phone.getText().toString());
    }

    public void click(View v)
    {
        if(v.getId()==R.id.button2)
        {
            Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+MainActivity.phone.getText().toString()));
            startActivity(i);
        }
    }
}
